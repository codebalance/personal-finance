<?php

function userRegistration() {
// Check is username is admin (will need to change this to approved boolean field) or if they are logged in.
// If not to either of these, redirect to login
// This is to handle user registration
if ($_SESSION["is_approved"] == 0 || !isset($_SESSION['loggedin'])) { 
    header("Location: ../auth/login.php");   
  }  
}

userRegistration(); 
?>