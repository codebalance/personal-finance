<?php

// If logged in session is not set, redirect to login page
// if(!isset($_SESSION['loggedin']) ){ 
//     header("Location: login.php");
//   }     
// Initialize the session
session_start();

    // Create session id variable
    $sess_id = $_SESSION['id'];

// Include user registration 
include 'user-registration.php';

// Create new MySQL connection
$mysqli = new mysqli('db764967104.hosting-data.io', 'dbo764967104', 'Q&jkr3E+', 'db764967104');

// Check that connection is made. Otherwise, fail connection
if (!$mysqli) die("Unable to connect to MySQL: " . mysql_error());  
 
// Define variables and initialize with empty values
$testImage = $image =  "";
 

    // Get image
    if(!empty($_FILES["image"]["name"])) {
        // Get file info
        $fileName = basename($_FILES["image"]["name"]);
        $fileType = pathinfo($fileName, PATHINFO_EXTENSION); 

        $allowTypes = array('jpg', 'png', 'jpeg', 'gif');

        if(in_array($fileType, $allowTypes)) {
            $testImage = $_FILES['image']['tmp_name'];
            $image = addslashes(file_get_contents($testImage)); 
 
        $insert = $mysqli->query("INSERT INTO userImages (sess_id, image) VALUES('$sess_id', '$image')");  

        } 
        if ($insert) {  
            $status = 'success'; 
            header("Location: ../index.php");
        } else {
            echo 'No success';  
        } 
    }
    
    // Close connection 
   // mysqli_close($link);
   $mysqli->close();
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add User Image</title>
    <link rel="shortcut icon" type="image/png" href="../img/wallet.png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>
</head>
<body>
    <div class="wrapper">
    <img src="../img/wallet.png">
        <h2>Add User Image</h2>
        <p>Please fill this form to add your personal user image. Thank you for your interest in our personal finance dashboard!</p>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" enctype="multipart/form-data">

            <div class="form-group"> 
                <label>Image</label>
                <input type="file" name="image" class="form-control">  
                <input type="submit" name="submit" value="Upload"> 
            </div>  

            <!-- <p>Already have an account? <a href="login.php">Login here</a>.</p> -->
        </form>
    </div>    
</body>
</html>