<?php

// If logged in session is not set, redirect to login page
// if(!isset($_SESSION['loggedin']) ){ 
//     header("Location: login.php");
//   }     
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome</title>
    <link rel="shortcut icon" type="image/png" href="../img/wallet.png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>
</head>
<body>
    <div class="wrapper">
    <img src="../img/wallet.png">
        <h2>Welcome</h2>
        <p>Looks like you do not have access to view this page. We apologize and thank you for working with us!</p>
        <p>Return? <a href="../index.php">Click here</a></p>
    </div>    
</body>
</html>