<?php

// If logged in session is not set, redirect to login page
// if(!isset($_SESSION['loggedin']) ){ 
//     header("Location: login.php");
//   }     
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Registration Awaiting Approval</title>
    <link rel="shortcut icon" type="image/png" href="../img/wallet.png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>
</head>
<body>
    <div class="wrapper">
    <img src="../img/wallet.png">
        <h2>Registration Awaiting Approval</h2>
        <p>You have created an account successfully! However, it looks like you do not have access to view this page as your registration has not been approved yet. We apologize and thank you for working with us!</p> 
        
    </div>    
</body>
</html>