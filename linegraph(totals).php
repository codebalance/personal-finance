<!DOCTYPE html>
<html>
  <head>
    <title>Totals - LineGraph</title>  
    <style>
      .chart-container {
        width: 750px;   
        height: auto;
      }
    </style>
  </head>
  <body>
    <div class="chart-container"> 
      <canvas id="mycanvas"></canvas> 
    </div>
    
    <!-- JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script type="text/javascript" src="js/linegraph(totals).js"></script>   
  </body>  
</html>     