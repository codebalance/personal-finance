$(document).ready(function(){
    $.ajax({
      url : "https://personal-finance.codebalance.org/totalsdata.php", 
      type : "GET",
      success : function(data){
        console.log(data);    
        // id, name, description, amount 
  
        var id = [];
        var name = [];
        var description = [];
        var amount = [];
  
        for(var i in data) {
          id.push("ID " + data[i].id);
          name.push(data[i].name);
          description.push(data[i].description);
          amount.push(data[i].amount);
        } 
  
        var chartdata = {
          labels: name,
          datasets: [
            {
              label: "Name",
              fill: false,
              lineTension: 0.1,
              backgroundColor: "rgba(59, 89, 152, 0.75)",
              borderColor: "rgba(59, 89, 152, 1)",
              pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
              pointHoverBorderColor: "rgba(59, 89, 152, 1)",
              data: name
            },
            {
              label: "description",
              fill: false,
              lineTension: 0.1,
              backgroundColor: "rgba(29, 202, 255, 0.75)",
              borderColor: "rgba(29, 202, 255, 1)", 
              pointHoverBackgroundColor: "rgba(29, 202, 255, 1)",
              pointHoverBorderColor: "rgba(29, 202, 255, 1)",
              data: description
            },
            {
              label: "amount",
              fill: false,
              lineTension: 0.1,
              backgroundColor: "rgba(211, 72, 54, 0.75)",
              borderColor: "rgba(211, 72, 54, 1)",
              pointHoverBackgroundColor: "rgba(211, 72, 54, 1)",
              pointHoverBorderColor: "rgba(211, 72, 54, 1)",
              data: amount
            }
          ]
        };
  
        var ctx = $("#mycanvas")[0].getContext("2d");  
  
        var LineGraph = new Chart(ctx, {
          type: 'line', 
          data: chartdata 
        });
      },
      error : function(data) {
  
      }
    });
  });