<!DOCTYPE html>
<html>
  <head>
    <title>Expenses - LineGraph</title>  
    <style>
      .chart-container {
        /* width: 640px; */
        height: auto;
      }

      @media only screen and (device-width: 768px) {
  /* For general iPad layouts */
}

@media only screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait) {
  /* For portrait layouts only */
  .chart-container {
          width: 750px; 
        }
}

@media only screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape) {
  /* For landscape layouts only */
  .chart-container {
          width: 900px;
        }
}
    </style>
  </head>
  <body>
    <div class="chart-container"> 
      <canvas id="mycanvas"></canvas> 
    </div>
    
    <!-- JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script type="text/javascript" src="js/linegraph(expenses).js"></script>   
  </body>   
</html>     