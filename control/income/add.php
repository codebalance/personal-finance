<?php
/*
Allows the user to both create new records and edit existing records
*/

// Initialize the session
session_start();

// If login session is not set, redirect to login page
// if(!isset($_SESSION['loggedin'])){ 
//   header("Location: ../auth/login.php"); 
// }  

   // connect to the database
   include('../../database/connect-db.php');  

   // creates the new/edit record form
// since this form is used multiple times in this file, I have made it a function that is easily reusable
   // function renderForm($name = '', $description ='', $amount = '', $date = '', $error = '', $id = '')

?>

<!DOCTYPE HTML>
<html lang="en">
<head> 
  <!-- Timeout after 5 mins of inactivity -->
  <meta http-equiv="refresh" content="300;url=../auth/logout.php" /> 
<title>
<?php $name = ''; $description =''; $amount = ''; $date = ''; $error = ''; $id = ''; if ($id != '') { echo "Edit Record"; } else { echo "New Record"; } ?>
</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <link rel="shortcut icon" type="image/png" href="../../img/wallet.png">

    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
<h1>New Record</h1>
<?php if ($error != '') {
echo "<div style='padding:4px; border:1px solid red; color:red'>" . $error
. "</div>"; 
} ?>

<form action="add(2).php" method="post"  onsubmit="return this">
<div class="wrapper">
<img src="../../img/wallet.png">

<div class="form-group" style="margin-top: 5%;">
<strong>Name: *</strong> <input type="text" class="form-control" name="name"
value="<?php echo $name; ?>"/> </div>

<div class="form-group">
<strong>Description: *</strong> <input type="text" class="form-control" name="description"
value="<?php echo $description; ?>"/> </div>

<div class="form-group">
<strong>Amount: *</strong> <input type="text" class="form-control" name="amount"
value="<?php echo $amount; ?>"/> </div>

<div class="form-group">
<strong>Date: *</strong> <input type="text" class="form-control" name="date"
value="<?php echo $date; ?>"/> </div> 

<p>* required</p>
<div class="form-group">
     <input type="submit" name="submit" class="btn btn-primary" value="Submit">
</div> 

</div>
</form>
</body>
</html>
