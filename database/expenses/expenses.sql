CREATE TABLE `db764967104`.expenses(
    `id` BINARY(16) PRIMARY KEY NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `description` VARCHAR(255) NOT NULL,
    `amount` VARCHAR(255) NOT NULL,
    `date` DATE NOT NULL,
    `created_at` TIMESTAMP NOT NULL
) ENGINE = MyISAM;