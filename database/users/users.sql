-- Use this to create your users table
CREATE TABLE `users` (
  `id` BINARY(16) PRIMARY KEY NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` TIMESTAMP NOT NULL,
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
  