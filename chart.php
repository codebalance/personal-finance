<?php
// This is to connect to the published IONOS 1&1 database, 6.28.21
$username = 'dbo764967104'; 
$password = 'Q&jkr3E+'; 
$host = 'db764967104.hosting-data.io';
$database = 'db764967104';

// Connect to database
$server = new mysqli($host, $username, $password, $database);
// $server = mysql_connect($host, $username, $password);
// $connection = mysql_select_db($database, $server);

// Perform query
// You may need to tune the query if your database is different
$myquery = "SELECT * FROM income; ";  

$query = mysqli_query($server, $myquery);  

if (!$query) {
    echo mysqli_error(); 
    die;
  } 

  // Create data object
$data = array();

for ($x = 0; $x < mysqli_num_rows($query); $x++) {
  $data[] = mysqli_fetch_assoc($query);
} 

// Encode data to json format
echo json_encode($data);
// return($data);

// Close connection
// $mysqli->close(); 

?>
<DOCTYPE html>
<html lang="en">
<head>
  <script src="https://cdn.anychart.com/releases/8.8.0/js/anychart-base.min.js"></script>
  <script src="https://cdn.anychart.com/releases/8.8.0/js/anychart-data-adapter.min.js"></script>
  <script src="https://cdn.anychart.com/releases/v8/js/anychart-base.min.js"></script>
  <script src="https://cdn.anychart.com/releases/v8/js/anychart-ui.min.js"></script>
  <script src="https://cdn.anychart.com/releases/v8/js/anychart-exports.min.js"></script>
  <link href="https://cdn.anychart.com/releases/v8/css/anychart-ui.min.css" type="text/css" rel="stylesheet">
  <link href="https://cdn.anychart.com/releases/v8/fonts/css/anychart-font.min.css" type="text/css" rel="stylesheet">
 
                
  <style type="text/css">
    html,
    body,
    #container { 
      width: 100%;
      height: 100%;
      margin: 0;
      padding: 0;
    }
  </style> 
</head>
<body>
  <div id="container"></div>
  <script>
    anychart.onDocumentReady(function () {
      anychart.data.loadJsonFile("php/data.php", function ($data) { 
        // create a chart and set loaded data
        chart = anychart.bar($data); 
        chart.animation(true);

        chart.padding([10, 40, 5, 20]); 

        chart.title('Top 10 Cosmetic Products by Revenue');

          // create bar series with passed data
  var series = chart.bar([
    ['Test'],
    ['View'] 
  ]);

  // set tooltip settings
  series
    .tooltip()
    .position('right')
    .anchor('left-center')
    .offsetX(5)
    .offsetY(0)
    .titleFormat('{%X}')
    .format('${%Value}');

  // set yAxis labels formatter
  chart.yAxis().labels().format('{%Value}{groupsSeparator: }');

  // set titles for axises
  chart.xAxis().title('Products by Revenue');
  chart.yAxis().title('Revenue in Dollars');
  chart.interactivity().hoverMode('by-x');
  chart.tooltip().positionMode('point');
  // set scale minimum
  chart.yScale().minimum(0);

        chart.container("container");
        chart.draw();
      });
    });
  </script>
</body>
</html>