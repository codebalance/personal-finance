<?php
/*
Allows the user to both create new records and edit existing records
*/
// Initialize the session
session_start();

// Include user registration
include '../../auth/user-registration.php'; 


   // connect to the database
   include('../database/connect-db.php');  

 
// creates the new/edit record form
// since this form is used multiple times in this file, I have made it a function that is easily reusable
// id, amount, rate, time, sess_id, use_totals, is_complete
function renderForm($amount = '', $rate ='', $time = '', $use_totals = '', $is_complete = '', $error = '', $id = '')
{ ?>
<!DOCTYPE HTML>
<html lang="en">
<head> 
  <!-- Timeout after 5 mins of inactivity -->
  <meta http-equiv="refresh" content="300;url=../auth/logout.php" /> 

  <link rel="shortcut icon" type="image/png" href="../../img/wallet.png"> 
<title>
Delete Record
</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <link rel="shortcut icon" type="image/png" href="../../img/wallet.png">

    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<style>

input:hover {
  cursor: pointer;
  }
  
</style>
</head>
<body>
<h1>Delete Record</h1>
<form action="" method="post">
<div class="wrapper">
<img src="../../img/wallet.png">

<?php if ($id != '') { ?> 
<input type="hidden" name="id" value="<?php echo $id; ?>" />
<?php } ?>

<div class="form-group" style="margin-top: 5%;">
<input type="hidden" class="form-control" name="amount"
value="<?php echo $amount; ?>"/> </div> 

<div class="form-group">
<input type="hidden" class="form-control" name="rate"
value="<?php echo $rate; ?>"/> </div>

<div class="form-group">
<input type="hidden" class="form-control" name="time"
value="<?php echo $time; ?>"/> </div>


<div class="form-group">
<!-- Create hidden input value to allow for and unchecked answer, meaning NOT to use in Totals view -->
<input name="use_totals" type="hidden" value="0" />
<input type="hidden" class="form-control" name="use_totals"
value="1" /> </div>    


<p style="color: red; font-size: 30px;">Are you sure you want to delete record, "<?php echo $id; ?>"?</p>
<!-- <p style="font-size: 25px;">***Be advised that all deleted income records will be saved in the Income History view. Enjoy!</p> -->

<div class="form-group">
     <input type="submit" name="submit" class="btn btn-primary" value="Submit">
</div> 

</div>
</form>
</body>
</html>

<?php }



/*

EDIT RECORD

*/
function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data; 
  }   

// Create new MySQL connection 
$mysqli = new mysqli('db764967104.hosting-data.io', 'dbo764967104', 'Q&jkr3E+', 'db764967104');

// if the 'id' variable is set in the URL, we know that we need to edit a record
if (isset($_GET['id']))
{
// if the form's submit button is clicked, we need to process the form
if (isset($_POST['submit']))
{
// make sure the 'id' in the URL is valid
if (is_numeric($_POST['id']))
{

    
// get variables from the URL/form
// $id = $_POST['id'];
// $name = htmlentities($_POST['name'], ENT_QUOTES);
// $description = htmlentities($_POST['description'], ENT_QUOTES);
// $amount = htmlentities($_POST['amount'], ENT_QUOTES);
// $date = htmlentities($_POST['date'], ENT_QUOTES);
// $use_totals = htmlentities($_POST['use_totals'], ENT_QUOTES); 
// $use_chart = htmlentities($_POST['use_chart'], ENT_QUOTES);


// id, amount, rate, time, sess_id, use_totals, is_complete
$amount = $_POST['amount'];
$rate = $_POST['rate']; 
$time = $_POST['time'];
$use_totals = $_POST['use_totals'];
$is_complete = $_POST['is_complete'];
$sess_id = $_SESSION['id']; 

// get the 'id' variable from the URL
$id = $_GET['id']; 

// check that name and description are both not empty
if ( $id == '') 
{
// if they are empty, show an error message and display the form
$error = 'ERROR: Please fill in all required fields!';
renderForm($amount, $rate, $time,  $use_totals, $is_complete, $error, $id);
}
else 
{
// // insert the new record into the database
// if ($stmt = $mysqli->prepare("INSERT INTO incomeHistory (name, description, amount, date, use_totals, use_chart, sess_id ) VALUES" . "('$name', '$description', '$amount', '$date', '$use_totals', '$use_chart', '$sess_id');"))
// { 
// $stmt->bind_param("ss", $name, $description, $amount, $date, $use_totals, $use_chart); 
// $stmt->execute();
// $stmt->close();    
// }
 
// delete record from database
if ($stmt2 = $mysqli->prepare("DELETE FROM interest WHERE id = ? LIMIT 1"))
{
$stmt2->bind_param("i",$id); 
$stmt2->execute(); 
$stmt2->close();
}
// show an error message if the query has an error
else
{
echo "ERROR: could not prepare SQL statement.";
}

// redirect the user once the form is updated
header("Location: interest.php"); 
}
}
// if the 'id' variable is not valid, show an error message
else
{
echo "Error!";
}
}
// if the form hasn't been submitted yet, get the info from the database and show the form
else
{
// make sure the 'id' value is valid
if (is_numeric($_GET['id']) && $_GET['id'] > 0)
{
// get 'id' from URL
$id = $_GET['id'];

// get the record from the database
// id, amount, rate, time, sess_id, use_totals, is_complete
if($stmt = $mysqli->prepare("SELECT id, amount, rate, time, sess_id, use_totals, is_complete FROM interest WHERE id=?"))
{
$stmt->bind_param("i", $id);
$stmt->execute();

$stmt->bind_result($id, $amount, $rate, $time, $use_totals, $is_complete);
$stmt->fetch();

// show the form
renderForm($amount, $rate, $time, $use_totals, $is_complete, NULL, $id);

$stmt->close();
}
// show an error if the query has an error
else
{
echo "Error: could not prepare SQL statement";
}
}
// if the 'id' value is not valid, redirect the user back to the view.php page
else
{
header("Location: interest.php");
}
}
} 

// close the mysqli connection
$mysqli->close(); 
?>  