<?php
/*
Allows the user to both create new records and edit existing records
*/

// Initialize the session
session_start();

// Include user registration
include '../../auth/user-registration.php'; 

   // connect to the database
   include('../database/connect-db.php');  

 

?>

<!DOCTYPE HTML>
<html lang="en">
<head> 
  <!-- Timeout after 5 mins of inactivity -->
  <meta http-equiv="refresh" content="300;url=../../auth/logout.php" /> 
<title>
<?php $name = ''; $description =''; $amount = ''; $tax = ''; $date = ''; $use_totals = ''; $use_chart = ''; $error = ''; $id = ''; if ($id != '') { echo "Edit Record"; } else { echo "New Record"; } ?>
</title> 
 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <link rel="shortcut icon" type="image/png" href="../../img/wallet.png">

    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; } 
    </style>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<style>
  input:hover {
  cursor: pointer;
  }

</style>
</head>
<body>
<h1><?php if ($id != '') { echo "Edit Record"; } else { echo "New Record"; } ?></h1> 
<?php if ($error != '') {
echo "<div style='padding:4px; border:1px solid red; color:red'>" . $error
. "</div>"; 
} ?>

<form action="add(2)" method="post"  onsubmit="return this"> 
<div class="wrapper">
<img src="../../img/wallet.png">

<div class="form-group" style="margin-top: 5%;">
<strong>Name: *</strong> <input type="text" class="form-control" name="name"
value="<?php echo $name; ?>" required/> </div>

<div class="form-group">
<strong>Description: *</strong> <input type="text" class="form-control" name="description"
value="<?php echo $description; ?>" required/> </div>

<div class="form-group">
<strong>Amount: *</strong> <input type="text" class="form-control" name="amount"
value="<?php echo $amount; ?>" required/> </div>


<div class="form-group">
<strong>Tax: *</strong> <input type="text" class="form-control" name="tax"
value="<?php echo $tax; ?>" required/> </div>

<div class="form-group">
<strong>Date: *</strong> <input type="date" style="cursor:pointer" class="form-control" name="date"
value="<?php echo $date; ?>" required/> </div> 

<div class="form-group">
<strong>Use in Totals? </strong>
<p>***This is used for what income(s) you want to display in your Totals view. Please use either <strong>keep unchecked</strong> for <strong>No</strong> or <strong>check the box</strong> for <strong>Yes</strong>. Thank you!</p>

<!-- Create hidden input value to allow for and unchecked answer, meaning NOT to use in Totals view -->
<input name="use_totals" type="hidden" value="0" />
<input type="checkbox" class="form-control" name="use_totals"
value="1" /> </div> 

<div class="form-group">
<strong>Use in Dashboard? </strong>
<p>***This is used for what income(s) you want to display in your Dashboard. Please use either <strong>keep unchecked</strong> for <strong>No</strong> or <strong>check the box</strong> for <strong>Yes</strong>. Thank you!</p>

<!-- Create hidden input value to allow for and unchecked answer, meaning NOT to use in Dashboard -->
<input name="use_chart"  type="hidden" value="0" />
<input type="checkbox" class="form-control" name="use_chart" 
value="1" /> </div> 

<p>* required</p>
<div class="form-group">
     <input type="submit" name="submit" class="btn btn-primary" value="Submit">
</div> 

</div>
</form>
</body>
</html>
