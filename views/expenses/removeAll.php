<?php
/*
Allows the user to both create new records and edit existing records
*/
// Initialize the session
session_start();

// Include user registration
include '../../auth/user-registration.php'; 


   // connect to the database
   include('../database/connect-db.php');  

 
// creates the new/edit record form
// since this form is used multiple times in this file, I have made it a function that is easily reusable
function renderForm($name = '', $description ='', $amount = '', $date = '', $use_totals = '', $use_chart = '', $error = '', $id = '')
{ ?>
<!DOCTYPE HTML>
<html lang="en">
<head> 
  <!-- Timeout after 5 mins of inactivity -->
  <meta http-equiv="refresh" content="300;url=../../auth/logout.php" /> 

  <link rel="shortcut icon" type="image/png" href="../img/wallet.png"> 
<title>
Update All Records for Totals and Dashboard
</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <link rel="shortcut icon" type="image/png" href="../../img/wallet.png">

    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<style>

input:hover {
  cursor: pointer;
  }
  
</style>
</head>
<body>
<h1>Update All Records for Totals and Dashboard</h1>
<form action="" method="post">
<div class="wrapper">
<img src="../../img/wallet.png">

<?php if ($id != '') { ?> 
<input type="hidden" name="id" value="<?php echo $id; ?>" />
<?php } ?>

<div class="form-group" style="margin-top: 5%;">
<input type="hidden" class="form-control" name="name"
value="<?php echo $name; ?>"/> </div> 

<div class="form-group">
<input type="hidden" class="form-control" name="description"
value="<?php echo $description; ?>"/> </div>

<div class="form-group">
<input type="hidden" class="form-control" name="amount"
value="<?php echo $amount; ?>"/> </div>

<div class="form-group">
<input type="hidden" class="form-control" name="date"
value="<?php echo $date; ?>"/> </div> 

<div class="form-group">
<!-- Create hidden input value to allow for and unchecked answer, meaning NOT to use in Totals view -->
<input name="use_totals" type="hidden" value="0" />
<input type="hidden" class="form-control" name="use_totals"
value="1" /> </div>    

<div class="form-group">
<!-- Create hidden input value to allow for and unchecked answer, meaning NOT to use in Totals view -->
<input name="use_chart" type="hidden" value="0" />
<input type="hidden" class="form-control" name="use_chart"
value="1" /> </div>   

<p style="color: red; font-size: 30px;">Are you sure you want to update all records, "<?php echo $name; ?>"?</p>
<p style="font-size: 25px;">***Be advised that all deleted expense records will be saved in the Expenses History view. Enjoy!</p>

<div class="form-group">
     <input type="submit" name="submit" class="btn btn-primary" value="Submit">
</div> 

</div>
</form>
</body>
</html>

<?php }



/*

EDIT RECORD

*/
function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data; 
  }   

// Create new MySQL connection 
$mysqli = new mysqli('db764967104.hosting-data.io', 'dbo764967104', 'Q&jkr3E+', 'db764967104');

// delete record from database
if ($stmt2 = $mysqli->prepare("UPDATE expenses set use_totals='', use_chart=''")) 
{
$stmt2->bind_param("i",$id); 
$stmt2->execute(); 
$stmt2->close();
}
// show an error message if the query has an error
else
{
echo "ERROR: could not prepare SQL statement.";
}

// redirect the user once the form is updated
header("Location: expenses"); 

 
// close the mysqli connection
$mysqli->close(); 
?>  