<?php
/*
Allows the user to both create new records and edit existing records
*/
// Initialize the session
session_start();

// Include user registration
include '../auth/user-registration.php'; 


   // connect to the database
   include('database/connect-db.php');  

 
// creates the new/edit record form
// since this form is used multiple times in this file, I have made it a function that is easily reusable
function renderForm($name = '', $description ='', $amount = '', $date = '', $use_totals = '', $use_chart = '', $error = '', $id = '')
{ ?>
<!DOCTYPE HTML>
<html lang="en">
<head> 
  <!-- Timeout after 5 mins of inactivity -->
  <meta http-equiv="refresh" content="300;url=../auth/logout.php" /> 

  <link rel="shortcut icon" type="image/png" href="../img/wallet.png"> 
<title>
<?php if ($id != '') { echo "Edit Record"; } else { echo "New Record"; } ?>
</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <link rel="shortcut icon" type="image/png" href="img/wallet.png">

    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<style>

input:hover {
  cursor: pointer;
  }
  
</style>
</head>
<body>
<h1><?php if ($id != '') { echo "Edit Record"; } else { echo "New Record"; } ?></h1>
<?php if ($error != '') {
echo "<div style='padding:4px; border:1px solid red; color:red'>" . $error
. "</div>";
} ?>

<form action="" method="post">
<div class="wrapper">
<img src="../../img/wallet.png">
<?php if ($id != '') { ?> 
<input type="hidden" name="id" value="<?php echo $id; ?>" />
<?php } ?>

<div class="form-group" style="margin-top: 5%;">
<strong>Name: *</strong> <input type="text" class="form-control" name="name"
value="<?php echo $name; ?>"/> </div>

<div class="form-group">
<strong>Description: *</strong> <input type="text" class="form-control" name="description"
value="<?php echo $description; ?>"/> </div>

<div class="form-group">
<strong>Amount: *</strong> <input type="text" class="form-control" name="amount"
value="<?php echo $amount; ?>"/> </div>

<div class="form-group">
<strong>Date: *</strong> <input type="date" class="form-control" name="date"
value="<?php echo $date; ?>"/> </div> 

<div class="form-group">
<strong>Use in Totals? </strong>
<p>***This is used for what expense(s) you want to display in your Totals view. Please use either <strong>keep unchecked</strong> for <strong>No</strong> or <strong>check the box</strong> for <strong>Yes</strong>. Thank you!</p>

<!-- Create hidden input value to allow for and unchecked answer, meaning NOT to use in Totals view -->
<input name="use_totals" type="hidden" value="0" />
<input type="checkbox" class="form-control" name="use_totals"
value="1" /> </div>  

<div class="form-group">
<strong>Use in Dashboard? </strong> 
<p>***This is used for what expense(s) you want to display in your Dashboard view. Please use either <strong>keep unchecked</strong> for <strong>No</strong> or <strong>check the box</strong> for <strong>Yes</strong>. Thank you!</p>

<!-- Create hidden input value to allow for and unchecked answer, meaning NOT to use in Totals view -->
<input name="use_chart" type="hidden" value="0" />
<input type="checkbox" class="form-control" name="use_chart"
value="1" /> </div>  
 
<p>* required</p>
<div class="form-group">
     <input type="submit" name="submit" class="btn btn-primary" value="Submit">
</div> 

</div>
</form>
</body>
</html>

<?php }



/*

EDIT RECORD

*/
// Create new MySQL connection 
$mysqli = new mysqli('db764967104.hosting-data.io', 'dbo764967104', 'Q&jkr3E+', 'db764967104');

// if the 'id' variable is set in the URL, we know that we need to edit a record
if (isset($_GET['id']))
{
// if the form's submit button is clicked, we need to process the form
if (isset($_POST['submit']))
{
// make sure the 'id' in the URL is valid
if (is_numeric($_POST['id']))
{
// get variables from the URL/form
$id = $_POST['id'];
$name = htmlentities($_POST['name'], ENT_QUOTES);
$description = htmlentities($_POST['description'], ENT_QUOTES);
$amount = htmlentities($_POST['amount'], ENT_QUOTES);
$date = htmlentities($_POST['date'], ENT_QUOTES);
$use_totals = htmlentities($_POST['use_totals'], ENT_QUOTES); 
$use_chart = htmlentities($_POST['use_chart'], ENT_QUOTES);

// check that name and description are both not empty
if ($name == '' || $description == '' || $amount == '' || $date == '') 
{
// if they are empty, show an error message and display the form
$error = 'ERROR: Please fill in all required fields!';
renderForm($name, $description, $amount, $date, $use_totals, $use_chart, $error, $id);
}
else
{
// if everything is fine, update the record in the database 
if ($stmt = $mysqli->prepare("UPDATE expenses set id='" . $_POST['id'] . "', name='" . $_POST['name'] . "', description='" . $_POST['description'] . "', amount='" . $_POST['amount'] . "', date='" . $_POST['date'] . "', use_totals='" . $_POST['use_totals'] . "', use_chart='" . $_POST['use_chart'] . "'  WHERE id='" . $_POST['id'] . "'")) 
{
$stmt->bind_param("ssi", $name, $description, $amount, $date, $use_totals, $use_chart, $id);
$stmt->execute();
$stmt->close();
} 
// show an error message if the query has an error
else 
{
echo "ERROR: could not prepare SQL statement.";
}

// redirect the user once the form is updated
header("Location: expenses.php"); 
}
}
// if the 'id' variable is not valid, show an error message
else
{
echo "Error!";
}
}
// if the form hasn't been submitted yet, get the info from the database and show the form
else
{
// make sure the 'id' value is valid
if (is_numeric($_GET['id']) && $_GET['id'] > 0)
{
// get 'id' from URL
$id = $_GET['id'];

// get the record from the database
if($stmt = $mysqli->prepare("SELECT id, name, description, amount, date, use_totals, use_chart FROM expenses WHERE id=?"))
{
$stmt->bind_param("i", $id);
$stmt->execute();

$stmt->bind_result($id, $name, $description, $amount, $date, $use_totals, $use_chart);
$stmt->fetch();

// show the form
renderForm($name, $description, $amount, $date, $use_totals, $use_chart, NULL, $id);

$stmt->close();
}
// show an error if the query has an error
else
{
echo "Error: could not prepare SQL statement";
}
}
// if the 'id' value is not valid, redirect the user back to the view.php page
else
{
header("Location: expenses.php");
}
}
}



/*

NEW RECORD
// This is where you actually do the update or insertion of new information for the update.

*/
// if the 'id' variable is not set in the URL, we must be creating a new record
else
{
// if the form's submit button is clicked, we need to process the form
if (isset($_POST['submit'])) 
{
// get the form data
$name = htmlentities($_POST['name'], ENT_QUOTES);
$description = htmlentities($_POST['description'], ENT_QUOTES);
$amount = htmlentities($_POST['amount'], ENT_QUOTES);
$date = htmlentities($_POST['date'], ENT_QUOTES);
$use_totals = htmlentities($_POST['use_totals'], ENT_QUOTES);
$use_chart = htmlentities($_POST['use_chart'], ENT_QUOTES);

// check that firstname and lastname are both not empty
if ($name == '' || $description == '' || $amount == '' || $date == '')
{
// if they are empty, show an error message and display the form
$error = 'ERROR: Please fill in all required fields!';
renderForm($name, $description, $amount, $date, $use_totals, $use_chart, $error); 
}
else
{
// insert the new record into the database
if ($stmt = $mysqli->prepare("INSERT INTO expenses (name, description, amount, date, use_totals, use_chart) VALUES (?, ?, ?, ?, ?, ?)"))
{
$stmt->bind_param("ss", $name, $description, $amount, $date, $use_totals, $use_chart); 
$stmt->execute(); 
$stmt->close(); 
}
// show an error if the query has an error
else
{
echo "ERROR: Could not prepare SQL statement.";
}
 
// redirect the user
header("Location: expenses.php");
}

}
// if the form hasn't been submitted yet, show the form
else
{
renderForm();
}
}

// close the mysqli connection
$mysqli->close();  
?> 