<?php
// Initialize the session
session_start();

// Check is username is admin (will need to change this to approved boolean field) or if they are logged in.
// This is to handle user registration
// For users, only super admin has access, so this needs additional logic
if ($_SESSION['is_approved'] == 0 || $_SESSION['id'] !== 1 || !isset($_SESSION['loggedin'])) { 
  header("Location: ../auth/login.php");   
}   

?> 
<!DOCTYPE html> 
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

   <!-- Timeout after 5 mins of inactivity -->
   <meta http-equiv="refresh" content="300;url=../auth/logout.php" /> 
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" type="image/png" href="../img/wallet.png">

  <title>Personal Finance Dashboard | Users</title>

 <!-- Custom fonts for this template -->
 <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <?php echo '<link rel="stylesheet" href="../css/sb-admin-2.css" />' ?>

  <!-- Custom styles for this page --> 
  <link href="../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">


</head> 

<body id="page-top" class="sidebar-toggled">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="../index.php">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-wallet"></i> 
        </div> 
      
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <!-- <li class="nav-item"> 
        <a class="nav-link" href="../index.php">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>
  -->
  

      
      <!-- Nav Item - Expenses -->
      <li class="nav-item">
        <a class="nav-link" href="expenses.php">
          <i class="fas fa-fw fa-chart-area"></i> 
          <span>Expenses</span></a>
      </li>

          <!-- Nav Item - Income -->
          <li class="nav-item">
        <a class="nav-link" href="income.php">
          <i class="fas fa-fw fa-chart-area"></i> 
          <span>Income</span></a>
      </li>

        <!-- Nav Item - Users -->
        <li class="nav-item">
        <a class="nav-link" href="users.php">
          <i class="fas fa-fw fa-chart-area"></i> 
          <span>Users</span></a>
      </li>

    
      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

     <!-- Include Topbar section -->
     <?php
    include 'partials/header(2).php';    
  

        ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

        <?php 
      // connect to the database
      include('../database/connect-db.php'); 

      // number of results to show per page
      $per_page = 50;
      
      // figure out the total pages in the database
if ($result = $mysqli->query("SELECT id, username, created_at, CASE WHEN is_approved = false THEN 'No' ELSE 'Yes' END from users; "))
{
if ($result->num_rows != 0) 
{ 
$total_results = $result->num_rows;

// ceil() returns the next highest integer value by rounding up value if necessary
$total_pages = ceil($total_results / $per_page);

// check if the 'page' variable is set in the URL (ex: view-paginated.php?page=1)
if (isset($_GET['page']) && is_numeric($_GET['page']))
{
$show_page = $_GET['page'];

// make sure the $show_page value is valid
if ($show_page > 0 && $show_page <= $total_pages)
{
$start = ($show_page -1) * $per_page;
$end = $start + $per_page;
}
else
{
// error - show first set of results
$start = 0;
$end = $per_page;
}
}
else
{
// if page isn't set, show first set of results
$start = 0;
$end = $per_page;
}

// display pagination
// echo "<p><a href='income(2).php'>View All</a> | <b>View Page:</b> ";
for ($i = 1; $i <= $total_pages; $i++)
{
if (isset($_GET['page']) && $_GET['page'] == $i)
{
echo $i . " ";
}
else
{
// echo "<a href='income(3).php?page=$i'>$i</a> ";
} 
}
echo "</p>"; 
echo " <a href='../auth/register.php'><i class='fas fa-plus fa-2x'></i></a>";

// display data in table
echo '<div class="card shadow mb-4">
                <div class="card-header py-3">
                </div>
                 <div class="card-body">
                 <div class="table-responsive">
                 <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0"><tr>
                 <th>ID</th>
                 <th>Username</th>
                 <th>Date Created</th>
                 <th>Approved</th>
                 <th>Edit</th>
                 <th>Delete</th>
                 </tr>'; 

// loop through results of database query, displaying them in the table
for ($i = $start; $i < $end; $i++)
{
// make sure that PHP doesn't try to show results that don't exist
if ($i == $total_results) { break; }

// find specific row
$result->data_seek($i);
$row = $result->fetch_row();
 
// echo out the contents of each row into a table 
echo "<tr>";
echo '<td>' . $row[0] . '</td>'; 
echo '<td>' . $row[1] . '</td>';
echo '<td>' . $row[2] . '</td>';
echo '<td>' . $row[3] . '</td>';
echo '<td><a href="update-user.php?id=' . $row[0] . '"><i class="fas fa-edit fa-2x"></i></a></td>';
echo '<td><a href="delete-user(2).php?id=' . $row[0] . '"><i class="fas fa-trash-alt fa-2x"></i></a></td>';
echo "</tr>";
}

// close table> 
echo "</table></div></div></div>";
}
else
{
echo "No results to display!";
}
}
// error with the query
else
{
echo "Error: " . $mysqli->error;
}

// close database connection
$mysqli->close();

        ?>  
     
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; John Curtis Jr, 2021</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="../auth/logout.php">Logout</a>
        </div>  
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="../vendor/jquery/jquery.min.js"></script>
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="../vendor/chart.js/Chart.min.js"></script>
 
  <!-- Page level custom scripts -->
  <script src="../js/demo/chart-area-demo.js"></script>
  <script src="../js/demo/chart-pie-demo.js"></script> 


  <!-- Page level plugins -->
  <script src="../vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="../vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="../js/demo/datatables-demo.js"></script>

</body>

</html>
