<?php
/*
Allows the user to both create new records and edit existing records
*/
// Initialize the session
session_start();

// Check is username is admin (will need to change this to approved boolean field) or if they are logged in.
// If not to either of these, redirect to login
// This is to handle user registration
// For users, only super admin has access, so this needs additional logic
if ($_SESSION['is_approved'] == 0 || $_SESSION['id'] !== 1 || !isset($_SESSION['loggedin'])) { 
  header("Location: ../auth/login.php");   
}   

   // connect to the database
   include('database/connect-db.php');  

 
// creates the new/edit record form
// since this form is used multiple times in this file, I have made it a function that is easily reusable
function renderForm($username = '', $is_approved = '', $error = '', $id = '')
{ ?>
<!DOCTYPE HTML>
<html lang="en">
<head> 
  <!-- Timeout after 5 mins of inactivity -->
  <meta http-equiv="refresh" content="300;url=../auth/logout.php" /> 

  <link rel="shortcut icon" type="image/png" href="../img/wallet.png"> 
<title>
<?php if ($id != '') { echo "Edit Record"; } else { echo "New Record"; } ?>
</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <link rel="shortcut icon" type="image/png" href="img/wallet.png">

    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<style>

input:hover {
  cursor: pointer;
  }
  
</style>
</head>
<body>
<h1><?php if ($id != '') { echo "Edit Record"; } else { echo "New Record"; } ?></h1>
<?php if ($error != '') {
echo "<div style='padding:4px; border:1px solid red; color:red'>" . $error
. "</div>";
} ?>

<form action="" method="post">
<div class="wrapper">
<img src="../../img/wallet.png">
<?php if ($id != '') { ?> 
<input type="hidden" name="id" value="<?php echo $id; ?>" />
<?php } ?>


<div class="form-group" style="margin-top: 5%;">
<strong>Username: *</strong> <input type="text" class="form-control" name="username"
value="<?php echo $username; ?>" required/> </div>

<div class="form-group">
<strong>Approved? *</strong> <input type="number" min="0" max="1" class="form-control" name="is_approved"
value="<?php echo $is_approved; ?>" required/> </div>


<p>* required</p>
<div class="form-group">
     <input type="submit" name="submit" class="btn btn-primary" value="Submit">
</div> 

</div>
</form>
</body>
</html>

<?php }



/*

EDIT RECORD

*/
// Create new MySQL connection 
$mysqli = new mysqli('db764967104.hosting-data.io', 'dbo764967104', 'Q&jkr3E+', 'db764967104');

// if the 'id' variable is set in the URL, we know that we need to edit a record
if (isset($_GET['id']))
{
// if the form's submit button is clicked, we need to process the form
if (isset($_POST['submit']))
{
// make sure the 'id' in the URL is valid
if (is_numeric($_POST['id']))
{
// get variables from the URL/form
$id = $_POST['id'];
$username = htmlentities($_POST['username'], ENT_QUOTES);
$is_approved = htmlentities($_POST['is_approved'], ENT_QUOTES);


// check that fields are not empty
if ($id == '' || $username == '' || $is_approved == '') 
{
// if they are empty, show an error message and display the form
$error = 'ERROR: Please fill in all required fields!';
renderForm($username, $is_approved, $error, $id); 
}
else
{
// if everything is fine, update the record in the database 
if ($stmt = $mysqli->prepare("UPDATE users set id='" . $_POST['id'] . "', username='" . $_POST['username'] . "', is_approved='" . $_POST['is_approved'] . "'  WHERE id='" . $_POST['id'] . "'")) 
{
$stmt->bind_param("ssi", $username, $is_approved, $id); 
$stmt->execute();
$stmt->close();
} 
// show an error message if the query has an error
else
{
echo "ERROR: could not prepare SQL statement(1).";
}

// redirect the user once the form is updated
header("Location: users.php"); 
}
}
// if the 'id' variable is not valid, show an error message
else
{
echo "Error!";
}
}
// if the form hasn't been submitted yet, get the info from the database and show the form
else
{
// make sure the 'id' value is valid
if (is_numeric($_GET['id']) && $_GET['id'] > 0)
{
// get 'id' from URL
$id = $_GET['id'];

// get the record from the database
if($stmt = $mysqli->prepare("SELECT id, username, is_approved FROM users WHERE id=?"))
{
$stmt->bind_param("i", $id);
$stmt->execute();

$stmt->bind_result($id, $username, $is_approved);
$stmt->fetch();

// show the form
renderForm($username, $is_approved, NULL, $id);

$stmt->close();
}
// show an error if the query has an error
else
{
echo "Error: could not prepare SQL statement(2)";
}
}
// if the 'id' value is not valid, redirect the user back to the view.php page
else
{
header("Location: users.php");
}
}
}



/*

NEW RECORD
// This is where you actually do the update or insertion of new information for the update.

*/
// if the 'id' variable is not set in the URL, we must be creating a new record
else
{
// if the form's submit button is clicked, we need to process the form
if (isset($_POST['submit'])) 
{
// get the form data
$id = $_POST['id'];
$username = htmlentities($_POST['username'], ENT_QUOTES);
$is_approved = htmlentities($_POST['is_approved'], ENT_QUOTES);

// check that firstname and lastname are both not empty
if ($username == '' || $is_approved == '')
{
// if they are empty, show an error message and display the form
$error = 'ERROR: Please fill in all required fields!';
renderForm($username, $is_approved, $error); 
}
else
{
// insert the new record into the database
if ($stmt = $mysqli->prepare("INSERT INTO users (username, is_approved) VALUES (?, ?)"))
{
$stmt->bind_param("ss", $username, $is_approved); 
$stmt->execute();
$stmt->close(); 
}
// show an error if the query has an error
else
{
echo "ERROR: Could not prepare SQL statement(3).";
}

// redirect the user
header("Location: users.php");
}

}
// if the form hasn't been submitted yet, show the form
else
{
renderForm();
}
}

// close the mysqli connection
$mysqli->close();  
?> 