<?php
// Check is username is admin (will need to change this to approved boolean field) or if they are logged in.
// If not to either of these, redirect to login
// This is to handle user registration
// For users, only super admin has access, so this needs additional logic
if ($_SESSION['is_approved'] == 0 || $_SESSION['id'] !== 1 || !isset($_SESSION['loggedin'])) { 
   header("Location: ../../auth/login.php");   
 }   

//  $test = header("Location: https://personal-finance.codebalance.org/views/admin/login.php");
//  if ($test == true) {
//    header("Location: https://personal-finance.codebalance.org/views/admin/users.php"); 
//  }

    header("HTTP/1.0 404 Not Found");

    echo '<html>
            <head>
                <meta http-equiv="Refresh" content="0;url=https://personal-finance.codebalance.org/views/admin/users.php" />
            </head><body></body>
          </html>';  


   // connect to the database
   include('../../database/connect-db.php');  

// confirm that the 'id' variable has been set
if (isset($_GET['id']) && is_numeric($_GET['id']))
{ 
// get the 'id' variable from the URL
$id = $_GET['id']; 

// delete record from database
if ($stmt = $mysqli->prepare("DELETE FROM users WHERE id = ? LIMIT 1"))
{
$stmt->bind_param("i",$id);  
$stmt->execute();
$stmt->close();
}
else
{
echo "ERROR: could not prepare SQL statement."; 
}
$mysqli->close();

// redirect user after delete is successful
header("Location: users.php");
}
else
// if the 'id' variable isn't set, redirect the user
{
header("Location: users.php");
}
 
?> 