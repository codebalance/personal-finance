<?php
// Initialize the session
session_start();

// Check is username is admin (will need to change this to approved boolean field) or if they are logged in.
// If not to either of these, redirect to login
// This is to handle user registration
// For users, only super admin has access, so this needs additional logic
if ($_SESSION['is_approved'] == 0  || !isset($_SESSION['loggedin'])) { 
  header("Location: ../auth/login.php");   
}   

// Get current year
$currentDate = date( 'l, m-d-y');  
$currentTime = date("h:i:sa");   
$year = date('Y');
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

   <!-- Timeout after 1 hour of inactivity -->
   <meta http-equiv="refresh" content="3600;url= ../auth/logout.php" /> 
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" type="image/png" href="../img/wallet.png"> 

  <title>Personal Finance Dashboard | Totals</title>

  <!-- Custom fonts for this template -->
  <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <?php echo '<link rel="stylesheet" href="../css/sb-admin-2.css" />' ?>

  <!-- Custom styles for this page --> 
  <link href="../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">


</head> 

<body id="page-top" class="sidebar-toggled">

  <!-- Page Wrapper -->
  <div id="wrapper">

     <!-- Include Topbar and Nav section -->
     <?php
    include 'partials/header(2).php';    
  

    ?>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Begin Page Content -->
        <div class="container-fluid">

        <?php 
        // Initialize the session
        session_start();

        // Create session id variable
         $sess_id = $_SESSION['id'];

      // connect to the database
      include('../database/connect-db.php');  

      // number of results to show per page
      $per_page = 50;
      
      // figure out the total pages in the database 
      // FORMAT(SUM((amount) * 0.77) / 12, 0)
if ($result = $mysqli->query("SELECT FORMAT(SUM((amount) * tax) / 12, 0), FORMAT(SUM(amount) * tax, 0) FROM `income` WHERE $sess_id = sess_id AND use_totals = 1;   
 ")) 
{
if ($result->num_rows != 0)  
{ 
$total_results = $result->num_rows;

// ceil() returns the next highest integer value by rounding up value if necessary
$total_pages = ceil($total_results / $per_page);

// check if the 'page' variable is set in the URL (ex: view-paginated.php?page=1)
if (isset($_GET['page']) && is_numeric($_GET['page']))
{
$show_page = $_GET['page'];

// make sure the $show_page value is valid
if ($show_page > 0 && $show_page <= $total_pages)
{
$start = ($show_page -1) * $per_page;
$end = $start + $per_page;
}
else
{
// error - show first set of results
$start = 0;
$end = $per_page;
}
}
else
{
// if page isn't set, show first set of results
$start = 0;
$end = $per_page;
}


for ($i = 1; $i <= $total_pages; $i++)
{
if (isset($_GET['page']) && $_GET['page'] == $i)
{
echo $i . " ";
}

}
echo "</p>";


// display data in table 
echo '<h2 style="color: #979797;"">Totals</h2>';
echo '<p style="color: #979797;""><strong>***</strong>Be advised that all reflected numbers are AFTER tax. Thank you!</p>'; 
echo '<div class="card shadow mb-4">
                <div class="card-header py-3">
                </div>
                 <div class="card-body">
                 <div class="table-responsive">
                 <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                 <tr>
                 <th>Income Total Monthly</th>
                 <th>Income Total Yearly</th>
                 </tr>'; 

// loop through results of database query, displaying them in the table
for ($i = $start; $i < $end; $i++)
{
// make sure that PHP doesn't try to show results that don't exist
if ($i == $total_results) { break; }

// find specific row
$result->data_seek($i);
$row = $result->fetch_row();
 
// echo out the contents of each row into a table 
echo "<tr>";
echo '<td>$' . $row[0] . '</td>';
echo '<td>$' . $row[1] . '</td>';
echo "</tr>";

 
}

// close table>
echo "</table></div></div></div>";
}

}
// error with the query
else
{
echo "Error: " . $mysqli->error;
}

// close database connection
$mysqli->close();

        ?> 


     
     
     <?php 
        // Initialize the session
        session_start();

        // Create session id variable
         $sess_id = $_SESSION['id'];

      // connect to the database
      include('../database/connect-db.php');  

      // number of results to show per page
      $per_page = 50;
      
      // figure out the total pages in the database 
if ($result2 = $mysqli->query("SELECT FORMAT(SUM(amount), 0), FORMAT(SUM(amount) * 12, 0) FROM `expenses` WHERE $sess_id = sess_id AND use_totals = 1;  
 ")) 
{
if ($result2->num_rows != 0) 
{ 
$total_results = $result2->num_rows;

// ceil() returns the next highest integer value by rounding up value if necessary
$total_pages = ceil($total_results / $per_page);

// check if the 'page' variable is set in the URL (ex: view-paginated.php?page=1)
if (isset($_GET['page']) && is_numeric($_GET['page']))
{
$show_page = $_GET['page'];

// make sure the $show_page value is valid
if ($show_page > 0 && $show_page <= $total_pages)
{
$start = ($show_page -1) * $per_page;
$end = $start + $per_page;
}
else
{
// error - show first set of results
$start = 0;
$end = $per_page;
}
}
else
{
// if page isn't set, show first set of results
$start = 0;
$end = $per_page;
}


for ($i = 1; $i <= $total_pages; $i++)
{
if (isset($_GET['page']) && $_GET['page'] == $i)
{
echo $i . " ";
}

}
echo "</p>";


// display data in table 
echo '<div class="card shadow mb-4">
                <div class="card-header py-3">
                </div>
                 <div class="card-body">
                 <div class="table-responsive">
                 <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                 <tr>
                 <th>Expense Total Monthly</th>
                 <th>Expense Total Yearly</th>
                 </tr>'; 

// loop through results of database query, displaying them in the table
for ($i = $start; $i < $end; $i++)
{
// make sure that PHP doesn't try to show results that don't exist
if ($i == $total_results) { break; }

// find specific row
$result2->data_seek($i);
$row2 = $result2->fetch_row();
 
// echo out the contents of each row into a table 
echo "<tr>";
echo '<td>$' . $row2[0] . '</td>';
echo '<td>$' . $row2[1] . '</td>';  
echo "</tr>";

 
}

// close table>
echo "</table></div></div></div>";
}

}
// error with the query
else
{
echo "Error: " . $mysqli->error;
}


// echo '<p>$' . $row[0] . '</p>'; 

// Let's get profits (income - expenses) and display in a separate table
if (1 == 1) {

  $result3 = $mysqli->query("SELECT SUM((amount) * tax) / 12 FROM `income` WHERE $sess_id = sess_id AND use_totals = 1;   
 ");

$result3->data_seek($i);
$row3 = $result3->fetch_row(); 

$result4 = $mysqli->query("SELECT SUM(amount) FROM `expenses` WHERE $sess_id = sess_id AND use_totals = 1;   
");

$result4->data_seek($i);
$row4 = $result4->fetch_row();


// var_dump($row3[0]); 
 
// First, create integer values as we need to convert income and expenses and interest from strings
$intIncome = (intval($row3[0])); 
// echo $intIncome; 

$intExpenses = (intval($row4[0]));
// echo $intExpenses;


// Get profits with finding difference between income and expenses
// and format that returned integer value
$profits = $intIncome - $intExpenses; 
$formatProfitsMonthly = number_format($profits); 
// echo $formatProfits;

$intIncomeYearly = $intIncome * 12; 
$intExpensesYearly = $intExpenses * 12;

$profitsYearly = $intIncomeYearly - $intExpensesYearly;
$formatProfitsYearly = number_format($profitsYearly); 

$profitsYearly2 = ($intIncomeYearly - $intExpensesYearly) * 2;
$formatProfitsYearly2 = number_format($profitsYearly2); 


$profitsYearly5 = ($intIncomeYearly - $intExpensesYearly) * 5;
$formatProfitsYearly5 = number_format($profitsYearly5); 

$profitsYearly10 = ($intIncomeYearly - $intExpensesYearly) * 10;
$formatProfitsYearly10 = number_format($profitsYearly10);  

$profitsYearly15 = ($intIncomeYearly - $intExpensesYearly) * 15;
$formatProfitsYearly15 = number_format($profitsYearly15); 

$profitsYearly20 = ($intIncomeYearly - $intExpensesYearly) * 20;
$formatProfitsYearly20 = number_format($profitsYearly20); 



// display data in table 
echo '<div class="card shadow mb-4">
                <div class="card-header py-3">
                </div>
                 <div class="card-body">
                 <div class="table-responsive">
                 <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                 <tr>
                 <th>Profits Total Monthly</th>
                 <th>Profits Total Yearly</th>
                 <th>Profits Total 2 Years</th>
                 <th>Profits Total 5 Years</th>
                 <th>Profits Total 10 Years</th>
                 <th>Profits Total 15 Years</th>
                 <th>Profits Total 20 Years</th>
                 </tr>'; 

// echo out the contents of each row into a table 
echo "<tr>"; 
echo '<td>$' . $formatProfitsMonthly . '</td>';
echo '<td>$' . $formatProfitsYearly . '</td>';
echo '<td>$' . $formatProfitsYearly2 . '</td>'; 
echo '<td>$' . $formatProfitsYearly5 . '</td>';
echo '<td>$' . $formatProfitsYearly10 . '</td>';
echo '<td>$' . $formatProfitsYearly15 . '</td>';
echo '<td>$' . $formatProfitsYearly20 . '</td>';
echo "</tr>";


// close table>
echo "</table></div></div></div>";

 } 

//  if ($result4 = $mysqli->query("SELECT SUM((amount) FROM `expenses` WHERE $sess_id = sess_id AND use_totals = 1;   
//  ")) {

// $result4->data_seek($i);
// $row3 = $result4->fetch_row(); 

// var_dump($row4[0]); 
 
// $intExpenses = (intval($row4[0])); 
// echo $intExpenses; 
//  }



 
// $result3 = $result - $result2;
// echo $result3; 

// close database connection
$mysqli->close();

        ?> 
     
     <?php 
        // Initialize the session
        session_start();

        // Create session id variable
         $sess_id = $_SESSION['id'];

      // connect to the database
      include('../database/connect-db.php');  

      // number of results to show per page
      $per_page = 50;

      // SELECT FORMAT(SUM((amount) * 0.77) / 12, 0), FORMAT(SUM(amount) * 0.77, 0), rate FROM `interest`
      
      // figure out the total pages in the database 
      // FORMAT(SUM((amount) * 0.77) / 12, 0)
      // compouned interest = principal (amount) * Power (1 + ( rate / 100 ),time);
if ($result5 = $mysqli->query("SELECT FORMAT(amount, 0), 
FORMAT(amount * Power(1 + (rate), 1) - amount, 0),
FORMAT(amount * Power(1 + (rate), 1), 0), 
FORMAT((amount + (amount * Power(1 + (rate), 1))) * Power(1 + (rate), 1), 0),  
FORMAT((amount + (amount + (amount * Power(1 + (rate), 1))) * Power(1 + (rate), 1)) * Power(1 + (rate), 1), 0),
FORMAT((amount + (amount + (amount + (amount + (amount * Power(1 + (rate), 1))))) * Power(1 + (rate), 1)) * Power(1 + (rate), 1) * Power(1 + (rate), 1) * Power(1 + (rate), 1), 0),
rate 
FROM `interest`
WHERE $sess_id = sess_id AND use_totals = 1;     
 ")) 
{
if ($result5->num_rows != 0) 
{ 
$total_results = $result5->num_rows;

// ceil() returns the next highest integer value by rounding up value if necessary
$total_pages = ceil($total_results / $per_page);

// check if the 'page' variable is set in the URL (ex: view-paginated.php?page=1)
if (isset($_GET['page']) && is_numeric($_GET['page']))
{
$show_page = $_GET['page'];

// make sure the $show_page value is valid
if ($show_page > 0 && $show_page <= $total_pages)
{
$start = ($show_page -1) * $per_page;
$end = $start + $per_page;
}
else
{
// error - show first set of results
$start = 0;
$end = $per_page;
}
}
else
{
// if page isn't set, show first set of results
$start = 0;
$end = $per_page;
}


for ($i = 1; $i <= $total_pages; $i++)
{
if (isset($_GET['page']) && $_GET['page'] == $i)
{
echo $i . " ";
}

}
echo "</p>";


// display data in table  
echo '<div class="card shadow mb-4">
                <div class="card-header py-3">
                </div>
                 <div class="card-body">
                 <div class="table-responsive">
                 <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                 <tr>
                 <th>Initial Amount Saved</th>
                 <th>Interest Accrued</th>
                 <th>Amount Saved After 1 Year (with Interest)</th>
                 <th>After 2 Years</th>
                 <th>After 3 Years</th>
                 <th>After 5 Years</th>
                 <th>Interest Rate</th>
                 </tr>'; 

// loop through results of database query, displaying them in the table
for ($i = $start; $i < $end; $i++)
{
// make sure that PHP doesn't try to show results that don't exist
if ($i == $total_results) { break; }

// find specific row
$result5->data_seek($i);
$row5 = $result5->fetch_row(); 
 
// echo out the contents of each row into a table 
echo "<tr>";
echo '<td>$' . $row5[0] . '</td>';
echo '<td>$' . $row5[1] . '</td>';
echo '<td>$' . $row5[2] . '</td>';
echo '<td>$' . $row5[3] . '</td>';
echo '<td>$' . $row5[4] . '</td>';
echo '<td>$' . $row5[5] . '</td>';
echo '<td>$' . $row5[6] . '</td>';
echo "</tr>";

 
}

// close table>
echo "</table></div></div></div>";
}

}
// error with the query
else
{
echo "Error: " . $mysqli->error;
}

// close database connection
$mysqli->close();

        ?> 
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

    <!-- Include Footer section -->
    <?php
    include '../partials/footer.php';  

        ?>
        <!-- End of Footer section -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="../auth/logout.php">Logout</a>
        </div>  
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="../vendor/jquery/jquery.min.js"></script>
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="../vendor/chart.js/Chart.min.js"></script>
 
  <!-- Page level custom scripts -->
  <script src="../js/demo/chart-area-demo.js"></script>
  <script src="../js/demo/chart-pie-demo.js"></script> 


  <!-- Page level plugins -->
  <script src="../vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="../vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="../js/demo/datatables-demo.js"></script>

</body>

</html>
