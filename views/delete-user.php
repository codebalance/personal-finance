<?php
/*
Allows the user to both create new records and edit existing records
*/
// Initialize the session
session_start();

// Include user registration
include '../auth/user-registration.php'; 


   // connect to the database
   include('database/connect-db.php');  

 
// creates the new/edit record form
// since this form is used multiple times in this file, I have made it a function that is easily reusable
function renderForm($username = '', $password ='', $created_at = '', $is_approved = '', $error = '', $id = '')
// id, username, password, created_at, is_approved
{ ?>
<!DOCTYPE HTML>
<html lang="en">
<head> 
  <!-- Timeout after 5 mins of inactivity -->
  <meta http-equiv="refresh" content="300;url=../auth/logout.php" /> 

  <link rel="shortcut icon" type="image/png" href="../img/wallet.png"> 
<title>
Delete Record
</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <link rel="shortcut icon" type="image/png" href="img/wallet.png">

    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<style>

input:hover {  
  cursor: pointer;
  }
  
</style>
</head>
<body>
<h1>Delete Record</h1>
<form action="" method="post">
<div class="wrapper">
<img src="../../img/wallet.png">

<!-- id, username, password, created_at, is_approved -->
<?php if ($id != '') { ?> 
<input type="hidden" name="id" value="<?php echo $id; ?>" />
<?php } ?>

<div class="form-group" style="margin-top: 5%;">
<input type="hidden" class="form-control" name="username"
value="<?php echo $username; ?>"/> </div> 

<div class="form-group" style="margin-top: 5%;">
<input type="hidden" class="form-control" name="password"
value="<?php echo $password; ?>"/> </div> 
 

<div class="form-group"> 
<input type="hidden" class="form-control" name="created_at"
value="<?php echo $created_at; ?>"/> </div>

<div class="form-group">
<input type="hidden" class="form-control" name="is_approved"
value="<?php echo $is_approved; ?>"/> </div>


<p style="color: red; font-size: 30px;">Are you sure you want to delete record, "<?php echo $username; ?>"?</p>
<p style="font-size: 25px;">***Be advised that all deleted users records will be saved in the Users History view. Enjoy!</p>

<div class="form-group">
     <input type="submit" name="submit" class="btn btn-primary" value="Submit">
</div> 

</div>
</form>
</body>
</html>

<?php }



/*

EDIT RECORD

*/
function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data; 
  }   

// Create new MySQL connection 
$mysqli = new mysqli('db764967104.hosting-data.io', 'dbo764967104', 'Q&jkr3E+', 'db764967104');

// if the 'id' variable is set in the URL, we know that we need to edit a record
if (isset($_GET['id']))
{
// if the form's submit button is clicked, we need to process the form
if (isset($_POST['submit']))
{
// make sure the 'id' in the URL is valid
if (is_numeric($_POST['id']))
{

    
// get variables from the URL/form
// $id = $_POST['id'];
// $name = htmlentities($_POST['name'], ENT_QUOTES);
// $description = htmlentities($_POST['description'], ENT_QUOTES);
// $amount = htmlentities($_POST['amount'], ENT_QUOTES);
// $date = htmlentities($_POST['date'], ENT_QUOTES);
// $use_totals = htmlentities($_POST['use_totals'], ENT_QUOTES); 
// $use_chart = htmlentities($_POST['use_chart'], ENT_QUOTES);

// id, username, password, created_at, is_approved

// $id = $_POST['id'];
$username = $_POST['username']; 
$password = $_POST['password']; 
// is_approved = $_POST['is_approved'];
$sess_id = $_SESSION['id'];  

// get the 'id' variable from the URL
$id = $_GET['id']; 

// check that name and description are both not empty
if ($username == '') 
{
// if they are empty, show an error message and display the form
$error = 'ERROR: Please fill in all required fields!';
renderForm($username, $password, $created_at, $is_approved, $error, $id);
}
else 
{
// insert the new record into the database
if ($stmt = $mysqli->prepare("INSERT INTO usersHistory (username, password, sess_id ) VALUES" . "('$username',  '$password', '$sess_id');"))
{ 
$stmt->bind_param("ss", $username, $password, $sess_id); 
$stmt->execute();
$stmt->close();    
}

// get the 'id' variable from the URL
$id = $_GET['id'];  
 
// delete record from database
if ($stmt2 = $mysqli->prepare("DELETE FROM users WHERE id = ? LIMIT 1"))
{
$stmt2->bind_param("i",$id); 
$stmt2->execute(); 
$stmt2->close();
}
// show an error message if the query has an error
else
{
echo "ERROR: could not prepare SQL statement.";
}

// redirect the user once the form is updated
header("Location: users.php"); 
}
}
// if the 'id' variable is not valid, show an error message
else
{
echo "Error!";
}
}
// if the form hasn't been submitted yet, get the info from the database and show the form
else
{
// make sure the 'id' value is valid
if (is_numeric($_GET['id']) && $_GET['id'] > 0)
{
// get 'id' from URL
$id = $_GET['id'];

// get the record from the database
if($stmt = $mysqli->prepare("SELECT id, username, password sess_id from users WHERE id=?"))
{
$stmt->bind_param("i", $id);
$stmt->execute();

$stmt->bind_result($id, $username, $password, $sess_id);
$stmt->fetch();

// show the form
renderForm($username, $password, $sess_id, NULL, $id);

$stmt->close();
}
// show an error if the query has an error
else
{
echo "Error: could not prepare SQL statement";
}
}
// if the 'id' value is not valid, redirect the user back to the view.php page
else
{
header("Location: users.php"); 
}
}
}  


// close the mysqli connection
$mysqli->close(); 
?>  