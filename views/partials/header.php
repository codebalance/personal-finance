       
       <?php

       // Initialize the session
session_start();

// Create session id variable
$sess_id = $_SESSION['id'];

// Create new MySQL connection
$mysqli = new mysqli('db764967104.hosting-data.io', 'dbo764967104', 'Q&jkr3E+', 'db764967104');

if ($result = $mysqli->query("SELECT image from userImages WHERE $sess_id = sess_id LIMIT 1; "))   
{
// find specific row  
$result->data_seek($i); 
$row = $result->fetch_row();  
} 
 
       // Set current date and time
$currentDate = date( 'l, m-d-y');  
$currentTime = date("h:i:sa");   

?>


  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-wallet"></i> 
        
        </div>
        <div class="sidebar-brand-text mx-3"></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <!-- <li class="nav-item active">
        <a class="nav-link" href="index.html">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li> -->

      <!-- Nav Item - Income -->
      <li class="nav-item">
        <a class="nav-link" href="views/income/income">  
        <i class="fas fa-money-check-alt"></i> 
          <span>Income</span></a> 
      </li>

      <!-- Nav Item - Income History -->
      <li class="nav-item">
        <a class="nav-link" href="views/income/incomeHistory"> 
          <i class="fas fa-history"></i>
          <span>Income History</span></a>
      </li>

      <!-- Nav Item - Expenses -->
      <li class="nav-item">
        <a class="nav-link" href="views/expenses/expenses">
        <i class="fas fa-file-invoice"></i> 
          <span>Expenses</span></a>
      </li>

      <!-- Nav Item - Expenses -->
      <li class="nav-item">
        <a class="nav-link" href="views/expenses/expensesHistory"> 
        <i class="fas fa-history"></i> 
          <span>Expenses History</span></a> 
      </li>

        <!-- Nav Item - Interests -->
        <li class="nav-item">
        <a class="nav-link" href="views/interest/interest">
        <i class="fas fa-comment-dollar"></i> 
          <span>Interest</span></a> 
      </li>

         <!-- Nav Item - Totals -->
         <li class="nav-item"> 
        <a class="nav-link" href="views/totals">
        <i class="fas fa-dollar-sign"></i> 
          <span>Totals</span></a>
      </li>

         <!-- Nav Item - Finances -->
         <?php if ($_SESSION['id'] === 1) { ?>
         <li class="nav-item"> 
        <a class="nav-link" href="finance">
        <i class="fas fa-dollar-sign"></i> 
          <span>Finances</span></a>
      </li>
      <?php  } ?>


        <!-- Nav Item - Users -->
      <?php if ($_SESSION['id'] === 1) { ?> 
      <li class="nav-item">
        <a class="nav-link" href="views/admin/users">
        <i class="fas fa-users"></i> 
          <span>Users</span></a>
      </li>

      <?php } ?>  

          <!-- Nav Item - Users History -->
          <!-- <li class="nav-item">
        <a class="nav-link" href="views/usersHistory">
          <i class="fas fa-fw fa-chart-area"></i> 
          <span>Users History</span></a>
      </li> -->

        <!-- Nav Item - Reset Password --> 
        <li class="nav-item">
        <a class="nav-link" href="auth/reset-password">
        <i class="fas fa-unlock"></i> 
          <span>Reset Password?</span></a> 
      </li>

           <!-- Nav Item - Add User Image? --> 
           <li class="nav-item">
        <a class="nav-link" href="auth/user-image">
        <i class="fas fa-images"></i> 
          <span>Add User Image?</span></a> 
      </li>

        <!-- Nav Item - Logout --> 
        <li class="nav-item">
        <a class="nav-link" href="auth/logout">
        <i class="fas fa-sign-out-alt"></i> 
          <span>Logout?</span></a> 
      </li>

    


      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content" style="background-color: white;">

               <!-- Topbar 
      navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow-->
      <nav class="navbar navbar-expand navbar-light topbar mb-4 static-top">
        
        <li class="nav-item dropdown no-arrow mx-1 navbar-nav ml-auto">
              <a class="nav-link" href="https://personal-finance.codebalance.org/" aria-expanded="false">
              You are Home. Welcome, <?php echo $_SESSION['username']; echo "!";  ?>
              </a> 
              
              <a class="nav-link" href="#" aria-expanded="false"> 
             / &nbsp; 
              </a>
              
            </li>

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button> 
          
              

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
           

              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline small" style="color: #979797;"><?php echo " Today is "; echo $currentDate; echo ", "; echo $currentTime; ?></span>     


                <img class="img-profile rounded-circle" src="data:image;charset=utf8;base64,<?php echo base64_encode($row[0]); ?>"> 

                <div class="topbar-divider d-none d-sm-block"></div>

                <!-- <a class="dropdown-item" href="auth/logout" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2"></i>
                </a> -->
              </a>

            </li>

            <li class="nav-item dropdown no-arrow mx-1">

            <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-cogs fa-2x"></i> 
              
              </a>
              <!-- Dropdown - Alerts -->
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                <h6 class="dropdown-header">
                  Settings
                </h6>
                <a class="dropdown-item d-flex align-items-center" href="auth/user-image">
                  <div class="mr-3">
                    <div class="icon-circle bg-primary"> 
                    <i class="fas fa-images fa-2x"></i>
                    </div>
                  </div>
                  <div>
                    <span class="font-weight-bold">Change User Image?</span>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="auth/reset-password">
                  <div class="mr-3">
                    <div class="icon-circle bg-success">
                    <i class="fas fa-unlock fa-2x"></i>
                    </div> 
                  </div>
                  <div>
                  <span class="font-weight-bold">Reset Password?</span>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="auth/logout">
                  <div class="mr-3">
                    <div class="icon-circle bg-warning">
                    <i class="fas fa-sign-out-alt fa-2x fa-fw mr-2"></i>
                    </div>
                  </div>
                  <div>
                  <span class="font-weight-bold">Logout?</span>
                  </div>
                </a>
                <a class="dropdown-item text-center small text-gray-500" href="#">Show All Settings</a>
              </div>
            </li>



            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link" href="auth/reset-password" aria-expanded="false">
              <i class="fas fa-unlock fa-2x"></i> 
              </a>              
            </li>

            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link" href="auth/logout" aria-expanded="false">
              <i class="fas fa-sign-out-alt fa-2x fa-fw mr-2"></i>
              </a>             
            </li>


          </ul>

        </nav>
        <hr>
        <!-- Begin Page Content -->
        <div class="container-fluid">

             <!-- <h2 style="margin-bottom: 5%; color: #979797;">Welcome, <?php// echo $_SESSION['username']; echo "!";  ?></h2>  -->
 
            <div> 
            <h2 style="color: #979797;">All Incomes </h2>
          <iframe id="scaled-frame" src="linegraph(income)" title="Income Linegraph"></iframe>  
            </div>  

            <div> 
            <h2 style="color: #979797;">All Expenses (Monthly)</h2>  
          <iframe id="scaled-frame" src="linegraph(expenses)" title="Expenses Linegraph"></iframe>
            </div>     

        </div>
        <!-- /.container-fluid -->
  
      </div>
      <!-- End of Main Content -->




    </div>
    <!-- End of Content Wrapper -->
    

  </div>
  <!-- End of Page Wrapper -->

   
        