       
       <?php

       // Initialize the session
session_start();

// Create session id variable
$sess_id = $_SESSION['id'];

// Create new MySQL connection
$mysqli = new mysqli('db764967104.hosting-data.io', 'dbo764967104', 'Q&jkr3E+', 'db764967104');

if ($result = $mysqli->query("SELECT image from userImages WHERE $sess_id = sess_id LIMIT 1; "))   
{
// find specific row  
$result->data_seek($i); 
$row = $result->fetch_row();  
} 
 
       // Set current date and time
$currentDate = date( 'l, m-d-y');  
$currentTime = date("h:i:sa");   

?>


  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-wallet"></i> 
        
        </div>
        <div class="sidebar-brand-text mx-3"></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <!-- <li class="nav-item active">
        <a class="nav-link" href="index.html">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li> -->

      <!-- Nav Item - Income -->
      <li class="nav-item">
        <a class="nav-link" href="views/income/income"> 
        <i class="fas fa-money-check-alt"></i> 
          <span>Income</span></a> 
      </li>

      <!-- Nav Item - Income History -->
      <li class="nav-item">
        <a class="nav-link" href="views/income/incomeHistory"> 
          <i class="fas fa-history"></i>
          <span>Income History</span></a>
      </li>

      <!-- Nav Item - Expenses -->
      <li class="nav-item">
        <a class="nav-link" href="views/expenses/expenses">
        <i class="fas fa-file-invoice"></i> 
          <span>Expenses</span></a>
      </li>

      <!-- Nav Item - Expenses -->
      <li class="nav-item">
        <a class="nav-link" href="views/expenses/expensesHistory"> 
        <i class="fas fa-history"></i> 
          <span>Expenses History</span></a> 
      </li>

        <!-- Nav Item - Interests -->
        <li class="nav-item">
        <a class="nav-link" href="views/interest/interest">
        <i class="fas fa-comment-dollar"></i> 
          <span>Interest</span></a> 
      </li>

         <!-- Nav Item - Totals -->
         <li class="nav-item"> 
        <a class="nav-link" href="views/totals">
        <i class="fas fa-dollar-sign"></i> 
          <span>Totals</span></a>
      </li>


        <!-- Nav Item - Users -->
      <?php if ($_SESSION['id'] === 1) { ?> 
      <li class="nav-item">
        <a class="nav-link" href="views/admin/users">
        <i class="fas fa-users"></i> 
          <span>Users</span></a>
      </li>

      <?php } ?>  

          <!-- Nav Item - Users History -->
          <!-- <li class="nav-item">
        <a class="nav-link" href="views/usersHistory">
          <i class="fas fa-fw fa-chart-area"></i> 
          <span>Users History</span></a>
      </li> -->

        <!-- Nav Item - Reset Password --> 
        <li class="nav-item">
        <a class="nav-link" href="auth/reset-password">
        <i class="fas fa-unlock"></i> 
          <span>Reset Password?</span></a> 
      </li>

           <!-- Nav Item - Add User Image? --> 
           <li class="nav-item">
        <a class="nav-link" href="auth/user-image">
        <i class="fas fa-images"></i> 
          <span>Add User Image?</span></a> 
      </li>

        <!-- Nav Item - Logout --> 
        <li class="nav-item">
        <a class="nav-link" href="auth/logout">
        <i class="fas fa-sign-out-alt"></i> 
          <span>Logout?</span></a> 
      </li>

    


      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content" style="background-color: white;">

               <!-- Topbar 
      navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow-->
      <nav class="navbar navbar-expand navbar-light topbar mb-4 static-top">
        
      <li class="nav-item dropdown no-arrow mx-1 navbar-nav ml-auto">
          <a class="nav-link" href="https://personal-finance.codebalance.org/" aria-expanded="false">
         Home <?php // echo $_SESSION['username']; echo "!";  ?>
          </a>  
          
          <a class="nav-link" href="<?php echo $link?>" aria-expanded="false"> 
         / &nbsp; <?php echo $title?>   
          </a> 
          
        </li>

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button> 
          
              

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
           

              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline small" style="color: #979797;"><?php echo " Today is "; echo $currentDate; echo ", "; echo $currentTime; ?></span>     


                <img class="img-profile rounded-circle" src="data:image;charset=utf8;base64,<?php echo base64_encode($row[0]); ?>"> 

                <div class="topbar-divider d-none d-sm-block"></div>

                <!-- <a class="dropdown-item" href="auth/logout" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2"></i>
                </a> -->
              </a>

            </li>

            <li class="nav-item dropdown no-arrow mx-1">

            <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-cogs fa-2x"></i> 
              
              </a>
              <!-- Dropdown - Alerts -->
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                <h6 class="dropdown-header">
                  Settings
                </h6>
                <a class="dropdown-item d-flex align-items-center" href="auth/user-image">
                  <div class="mr-3">
                    <div class="icon-circle bg-primary"> 
                    <i class="fas fa-images fa-2x"></i>
                    </div>
                  </div>
                  <div>
                    <span class="font-weight-bold">Change User Image?</span>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="auth/reset-password">
                  <div class="mr-3">
                    <div class="icon-circle bg-success">
                    <i class="fas fa-unlock fa-2x"></i>
                    </div> 
                  </div>
                  <div>
                  <span class="font-weight-bold">Reset Password?</span>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="auth/logout">
                  <div class="mr-3">
                    <div class="icon-circle bg-warning">
                    <i class="fas fa-sign-out-alt fa-2x fa-fw mr-2"></i>
                    </div>
                  </div>
                  <div>
                  <span class="font-weight-bold">Logout?</span>
                  </div>
                </a>
                <a class="dropdown-item text-center small text-gray-500" href="#">Show All Settings</a>
              </div>
            </li>



            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link" href="auth/reset-password" aria-expanded="false">
              <i class="fas fa-unlock fa-2x"></i> 
              </a>              
            </li>

            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link" href="auth/logout" aria-expanded="false">
              <i class="fas fa-sign-out-alt fa-2x fa-fw mr-2"></i>
              </a>             
            </li>


          </ul>

        </nav>
        <hr>
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <?php
                  $curl = curl_init();

                  curl_setopt_array($curl, [
                    CURLOPT_URL => "https://google-finance4.p.rapidapi.com/search/?q=airbnb&hl=en&gl=US",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => [
                      "x-rapidapi-host: google-finance4.p.rapidapi.com",
                      "x-rapidapi-key: a79e31d717msh9e9569de7ac7ac6p1c3e0bjsna7d0403b30b7"
                    ],
                  ]);
          
                  $response = curl_exec($curl);
                  $err = curl_error($curl);
          
                  curl_close($curl);
          
                  if ($err) {
                    echo "cURL Error #:" . $err;
                  } else {

                      // Connect to the database
                      include('../../database/connect-db.php'); 


                            // number of results to show per page
      $per_page = 5;
      
      // figure out the total pages in the database
if ($result = $mysqli->query("SELECT id, name, description, concat('$', format((amount), 0)), tax, date, CASE WHEN use_totals = 0 THEN 'No' ELSE 'Yes' END, CASE WHEN use_chart = 0 THEN 'No' ELSE 'Yes' END from income WHERE $sess_id = sess_id ORDER BY date DESC; ")) 
{
if ($result->num_rows != 0)   
{ 
$total_results = $result->num_rows;

// ceil() returns the next highest integer value by rounding up value if necessary
$total_pages = ceil($total_results / $per_page);

// check if the 'page' variable is set in the URL (ex: view-paginated.php?page=1)
if (isset($_GET['page']) && is_numeric($_GET['page']))
{
$show_page = $_GET['page'];

// make sure the $show_page value is valid
if ($show_page > 0 && $show_page <= $total_pages) 
{
$start = ($show_page -1) * $per_page;
$end = $start + $per_page;
}
else
{
// error - show first set of results
$start = 0;
$end = $per_page;
}
}
else
{
// if page isn't set, show first set of results
$start = 0; 
$end = $per_page;
}

if ($result2 = $mysqli->query("SELECT COUNT(*) from income WHERE $sess_id = sess_id; "))   
{
  // find specific row  
$result2->data_seek($i); 
$row2 = $result2->fetch_row();  
} 



// display pagination
// $row2[0] 
 echo "<a href='income(2)'>View all $row2[0] record(s) </a> | <a href='add'><i class='fas fa-plus fa-2x'></i></a>"; 
 
// for ($i = 1; $i <= $total_pages; $i++) 
// {
// if (isset($_GET['page']) && $_GET['page'] == $i)
// {
// echo $i . " ";
// }
// else
// {
// echo "<a href='income(3).php?page=$i'>$i</a> ";
// } 
// }
// echo "</p>";
// echo "";

// display data in table 
echo '<div class="card shadow mb-4">
                <div class="card-header py-3">
                </div>
                 <div class="card-body">
                 <div class="table-responsive">
                 <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0"><tr>
                 <th>ID</th>
                 <th>Name</th>
                 <th>Description</th>
                 <th>Amount</th>
                 <th>Tax</th>
                 <th>Date</th>
                 <th>Use in Totals?</th>
                 <th>Use in Dashboard?</th>
                 <th>Edit</th>
                 <th>Delete</th>
                 </tr>'; 

// loop through results of database query, displaying them in the table
for ($i = $start; $i < $end; $i++)
{
// make sure that PHP doesn't try to show results that don't exist
if ($i == $total_results) { break; }

// find specific row
$result->data_seek($i);
$row = $result->fetch_row();
 
// echo out the contents of each row into a table 
echo "<tr>";
echo '<td>' . $row[0] . '</td>';
echo '<td>' . $row[1] . '</td>';
echo '<td>' . $row[2] . '</td>';
echo '<td>' . $row[3] . '</td>'; 
echo '<td>' . $row[4] . '</td>';  
echo '<td>' . $row[5] . '</td>'; 
echo '<td>' . $row[6] . '</td>'; 
echo '<td>' . $row[7] . '</td>';
echo '<td><a href="update.php?id=' . $row[0] . '"><i class="fas fa-edit fa-2x"></i></a></td>';
echo '<td><a href="delete.php?id=' . $row[0] . '"><i class="fas fa-trash-alt fa-2x"></i></a></td>';
echo "</tr>";
} 

// close table>
echo "</table></div></div></div>";
}
else
{
echo "No results to display!";
echo " <a href='add'><i class='fas fa-plus fa-2x'></i></a>";
}
}


                       // Decoding JSON data
                        // $decodedData = 
                        // json_decode($response, true); 

                        // Encode with JSON
                        $data =
                        json_decode($response, true);  
                        

                       // print_r($data);   

                 
                  } 
                  if (1 == 1) {
                    // Open the table
                    echo "<table>";
            
                    // Cycle through the array 
                    foreach ($data->info as $idx => $info) {
            
                        // Output a row
                        echo "<tr>";
                        echo "<td>$info->type</td>";
                        echo "<td>$info->title</td>";
                        echo "</tr>";
                    }
            
                    // Close the table
                    echo "</table>";
                }

          

          ?>

          <table>
            <thead>
              <tr>
                <td>Title</td>
              </tr>
            </thead>
            <tbody>
              <?php foreach($MyObjectMap as $key => $item): ?>
                <tr> 
                  <td><?php echo $item->type; ?></td>
                  
               </tr>
               <?php endforeach; ?>
            </tbody>
          
          </table>

             <!-- <h2 style="margin-bottom: 5%; color: #979797;">Welcome, <?php// echo $_SESSION['username']; echo "!";  ?></h2>  -->
   

        </div>
        <!-- /.container-fluid -->
  
      </div>
      <!-- End of Main Content -->




    </div>
    <!-- End of Content Wrapper -->
    

  </div>
  <!-- End of Page Wrapper -->

   
        