        <!-- Topbar 
      navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow-->
      <nav class="navbar navbar-expand navbar-light topbar mb-4 static-top">
        
        <li class="nav-item dropdown no-arrow mx-1 navbar-nav ml-auto">
              <a class="nav-link" href="https://personal-finance.codebalance.org/index.php" aria-expanded="false">
              You are Home. Welcome, <?php echo $_SESSION['username']; echo "!";  ?>
              </a> 
              
              <a class="nav-link" href="#" aria-expanded="false"> 
             / &nbsp; 
              </a>
              
            </li>

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button> 
          
              

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
           

              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline small" style="color: #979797;"><?php echo " Today is "; echo $currentDate; echo ", "; echo $currentTime; ?></span>     


                <img class="img-profile rounded-circle" src="data:image;charset=utf8;base64,<?php echo base64_encode($row[0]); ?>"> 

                <div class="topbar-divider d-none d-sm-block"></div>

                <!-- <a class="dropdown-item" href="auth/logout.php" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2"></i>
                </a> -->
              </a>

            </li>

            <li class="nav-item dropdown no-arrow mx-1">

            <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-cogs fa-2x"></i> 
              
              </a>
              <!-- Dropdown - Alerts -->
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                <h6 class="dropdown-header">
                  Settings
                </h6>
                <a class="dropdown-item d-flex align-items-center" href="auth/user-image.php">
                  <div class="mr-3">
                    <div class="icon-circle bg-primary"> 
                    <i class="fas fa-images fa-2x"></i>
                    </div>
                  </div>
                  <div>
                    <span class="font-weight-bold">Change User Image?</span>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="auth/reset-password.php">
                  <div class="mr-3">
                    <div class="icon-circle bg-success">
                    <i class="fas fa-unlock fa-2x"></i>
                    </div> 
                  </div>
                  <div>
                  <span class="font-weight-bold">Reset Password?</span>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="auth/logout.php">
                  <div class="mr-3">
                    <div class="icon-circle bg-warning">
                    <i class="fas fa-sign-out-alt fa-2x fa-fw mr-2"></i>
                    </div>
                  </div>
                  <div>
                  <span class="font-weight-bold">Logout?</span>
                  </div>
                </a>
                <a class="dropdown-item text-center small text-gray-500" href="#">Show All Settings</a>
              </div>
            </li>



            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link" href="auth/reset-password.php" aria-expanded="false">
              <i class="fas fa-unlock fa-2x"></i> 
              </a>              
            </li>

            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link" href="auth/logout.php" aria-expanded="false">
              <i class="fas fa-sign-out-alt fa-2x fa-fw mr-2"></i>
              </a>             
            </li>


          </ul>

        </nav>
        <hr>